﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace YodleeDomain.Migrations
{
    public partial class FixedHoldingsProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ValuePrice",
                table: "Holdings",
                newName: "ValueCurrency");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ValueCurrency",
                table: "Holdings",
                newName: "ValuePrice");
        }
    }
}
