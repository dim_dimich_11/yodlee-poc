﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace YodleeDomain.Migrations
{
    public partial class InitLocalMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Accounts_OutsideAssetConnectionId",
                table: "Accounts",
                column: "OutsideAssetConnectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_OutsideAssetConnections_OutsideAssetConnectionId",
                table: "Accounts",
                column: "OutsideAssetConnectionId",
                principalTable: "OutsideAssetConnections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_OutsideAssetConnections_OutsideAssetConnectionId",
                table: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_OutsideAssetConnectionId",
                table: "Accounts");
        }
    }
}
