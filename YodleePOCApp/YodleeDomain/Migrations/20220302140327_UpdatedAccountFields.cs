﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace YodleeDomain.Migrations
{
    public partial class UpdatedAccountFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProviderAccountId",
                table: "Accounts",
                newName: "OutsideAssetConnectionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OutsideAssetConnectionId",
                table: "Accounts",
                newName: "ProviderAccountId");
        }
    }
}
