﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace YodleeDomain.Migrations
{
    public partial class UpdatedAssetConnection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InstitutionId",
                table: "OutsideAssetConnections");

            migrationBuilder.RenameColumn(
                name: "InstitutionName",
                table: "OutsideAssetConnections",
                newName: "ProviderName");

            migrationBuilder.AddColumn<long>(
                name: "ProviderAccountId",
                table: "OutsideAssetConnections",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "ProviderId",
                table: "OutsideAssetConnections",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProviderAccountId",
                table: "OutsideAssetConnections");

            migrationBuilder.DropColumn(
                name: "ProviderId",
                table: "OutsideAssetConnections");

            migrationBuilder.RenameColumn(
                name: "ProviderName",
                table: "OutsideAssetConnections",
                newName: "InstitutionName");

            migrationBuilder.AddColumn<string>(
                name: "InstitutionId",
                table: "OutsideAssetConnections",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
