﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace YodleeDomain.Migrations
{
    public partial class UpdatedYodleeAccountIdProverty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProviderAccountId",
                table: "Accounts",
                newName: "YodleeAccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "YodleeAccountId",
                table: "Accounts",
                newName: "ProviderAccountId");
        }
    }
}
