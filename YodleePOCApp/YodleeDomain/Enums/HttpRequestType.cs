﻿namespace YodleeDomain.Enums
{
    public enum HttpRequestType
    {
        Get,
        Post,
        Put,
        Delete,
        Patch
    }
}
