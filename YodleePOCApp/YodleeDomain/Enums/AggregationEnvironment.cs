﻿namespace YodleeDomain.Enums
{
    public enum AggregationEnvironment
    {
        Sandbox,
        Launch,
        Production
    }
}
