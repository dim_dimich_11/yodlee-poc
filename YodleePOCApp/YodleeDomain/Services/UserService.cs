﻿using AutoMapper;
using Newtonsoft.Json;
using YodleeDomain.Entities;
using YodleeDomain.Models;
using YodleeDomain.Models.RequestModels;
using YodleeDomain.Models.ResponseModels;
using YodleeDomain.Repositories.Interfaces;
using YodleeDomain.Services.Interfaces;

namespace YodleeDomain.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _repository;
        private readonly IRequestSenderService _requestSenderService;
        private readonly IMapper _mapper;

        public UserService(IRepository<User> repository, IRequestSenderService requestSenderService, IMapper mapper)
        {
            _repository = repository;
            _requestSenderService = requestSenderService;
            _mapper = mapper;
        }

        public async Task<List<User>> GetAllAsync()
        {
            return await _repository.GetAllAsync(x => true);
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            User result = await _repository.GetAsync(x => x.Id == userId);

            return result;
        }

        public async Task<DatabaseResult> UpdateUserAsync(User user)
        {
            DatabaseResult result = await _repository.UpdateAsync(user);

            return result;
        }

        public async Task<RequestResult<object>> SaveUserAsync(User user)
        {
            var result = new RequestResult<object>();

            var authAdminRequest = new AuthRequest(Constants.AdminLoginName);

            var getTokenResult = await _requestSenderService.SendRequestAsync<AuthResponse>(authAdminRequest, true);

            if (!getTokenResult.IsSuccess)
            {
                result.Error = getTokenResult.Error;

                return result;
            }

            var yodleeUser = _mapper.Map<YodleeUser>(user);
            var registerUserRequestModel = new RegisterUserRequestModel
            {
                User = yodleeUser
            };

            var registerUserRequest = new RegisterUserRequest(getTokenResult.Result.Token.AccessToken)
            {
                BodyJson = JsonConvert.SerializeObject(registerUserRequestModel)
            };

            var registerUserResult = await _requestSenderService.SendRequestAsync<RegisterUserResponseModel>(registerUserRequest);

            if (!registerUserResult.IsSuccess)
            {
                result.Error = registerUserResult.Error;

                return result;
            }

            user.YodleeUserId = registerUserResult.Result.User.Id;
            user.YodleeUserLoginName = registerUserResult.Result.User.LoginName;
            user.YodleeUserRoleType = registerUserResult.Result.User.RoleType;
            user.WebhookCallbackUrl = Constants.WebhookUrl;

            DatabaseResult insertResult = await _repository.InsertAsync(user);

            if (!insertResult.Success)
            {
                result.Error = _mapper.Map<RequestError>(insertResult);
                return result;
            }

            return result;
        }
    }
}
