﻿using System.Text;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YodleeDomain.Enums;
using YodleeDomain.Extensions;
using YodleeDomain.Models;
using YodleeDomain.Services.Interfaces;

namespace YodleeDomain.Services
{
    public class RequestSenderService : IRequestSenderService
    {
        private readonly HttpClient _client;
        private readonly AssetAggregationConfig _aggConfigs;
        private readonly KeyValuePair<string, string>[] _authParams;

        public RequestSenderService(IHttpClientFactory clientFactory, IOptions<AssetAggregationConfig> aggConfigs)
        {
            _client = clientFactory.CreateClient(Constants.YodleeHttpClient);
            _aggConfigs = aggConfigs.Value;
            _authParams = new[]
            {
                new KeyValuePair<string, string>("clientId", _aggConfigs.ClientId),
                new KeyValuePair<string, string>("secret", _aggConfigs.Secret)
            };
        }

        public async Task<RequestResult<TResponse>> SendRequestAsync<TResponse>(RequestBase request, bool authorize = false)
        {
            var result = new RequestResult<TResponse>();
            string url = $"{_aggConfigs.DomainUrl}{request.RequestUrl}";

            // add url parameters for the request
            url = url.AddUrlParameters(request.RequestUrParameters);
            string requestJson = request.ToJson();
            
            // add headers for the request
            _client.AddHeaders(request.RequestHeaders);

            using (HttpContent content = authorize
                       ? new FormUrlEncodedContent(_authParams)
                       : GenerateContent(request, request.RequestType))
            using (HttpResponseMessage response = await GetResponseAsync(url, content, request.RequestType))
            {
                string jsonResponse = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    JObject error = JObject.Parse(jsonResponse);
                    result.Error = new RequestError
                    {
                        ErrorCode = error["errorCode"]?.ToString(),
                        ErrorMessage = error["errorMessage"]?.ToString(),
                        ReferenceCode = error["referenceCode"]?.ToString()
                    };
                }
                else
                {
                    result.Result = JsonConvert.DeserializeObject<TResponse>(jsonResponse);
                }
            }

            return result;
        }

        private async Task<HttpResponseMessage> GetResponseAsync(string url, HttpContent content, HttpRequestType requestType)
        {
            HttpResponseMessage response = null;

            switch (requestType)
            {
                case HttpRequestType.Get:
                    response = await _client.GetAsync(url);
                    break;
                case HttpRequestType.Post:
                    response = await _client.PostAsync(url, content);
                    break;
                case HttpRequestType.Put:
                    response = await _client.PutAsync(url, content);
                    break;
                case HttpRequestType.Delete:
                    response = await _client.DeleteAsync(url);
                    break;
                case HttpRequestType.Patch:
                    response = await _client.PatchAsync(url, content);
                    break;
            }

            return response;
        }

        private HttpContent GenerateContent(RequestBase request, HttpRequestType requestType)
        {
            HttpContent content;

            if (requestType is HttpRequestType.Get or HttpRequestType.Delete)
            {
                content = new StringContent(string.Empty);
                return content;
            }

            string contentValue = requestType is HttpRequestType.Post ? request.BodyJson : request.ToJson();

            content = new StringContent(contentValue, Encoding.UTF8, Constants.ContentApplicationJson);

            return content;
        }
    }
}
