﻿using AutoMapper;
using YodleeDomain.Entities;
using YodleeDomain.Models;
using YodleeDomain.Models.RequestModels;
using YodleeDomain.Models.ResponseModels;
using YodleeDomain.Repositories.Interfaces;
using YodleeDomain.Services.Interfaces;

namespace YodleeDomain.Services
{
    public class AccountsService : IAccountsService
    {
        private readonly IRequestSenderService _requestService;
        private readonly IHoldingsService _holdingsService;
        private readonly IRepository<Account> _accountRepository;
        private readonly IRepository<OutsideAssetConnection> _connectionRepository;
        private readonly IMapper _mapper;

        public AccountsService(IRepository<Account> accountRepository, IRepository<OutsideAssetConnection> connectionRepository, 
            IRequestSenderService requestService, IHoldingsService holdingsService, IMapper mapper)
        {
            _requestService = requestService;
            _holdingsService = holdingsService;
            _accountRepository = accountRepository;
            _connectionRepository = connectionRepository;
            _mapper = mapper;
        }

        public async Task<List<Account>> GetAccountsAsync(int connectionId)
        {
            var result = await _accountRepository.GetAllAsync(x => x.OutsideAssetConnectionId == connectionId,
                nameof(OutsideAssetConnection));

            return result;
        }

        public async Task<DatabaseResult> SaveAccountsAsync(long providerAccountId, string accessToken, int connectionId)
        {
            var request = new GetAccountsRequest(accessToken, providerAccountId);
            DatabaseResult result = null;

            var response = await _requestService.SendRequestAsync<GetAccountsResponse>(request);

            if (response.IsSuccess)
            {
                List<Account> mapResult = _mapper.Map<List<AssetAccount>, List<Account>>(response.Result.Account, x =>
                    x.AfterMap(
                        (source, dest) =>
                        {
                            dest.ForEach(a => a.OutsideAssetConnectionId = connectionId);
                        }));

                result = await _accountRepository.InsertRangeAsync(mapResult);

                if (result.Success)
                {
                    result = await _holdingsService.SaveHoldingsAsync(mapResult, accessToken);
                }
            }

            return result;
        }

        public async Task<RequestResult<DeleteAccountResponse>> DeleteAccountAsync(int accountId, string loginName, int connectionId)
        {
            var result = new RequestResult<DeleteAccountResponse>
            {
                Result = new DeleteAccountResponse()
            };

            var authRequest = new AuthRequest(loginName);

            // checking if account with such id exist in our database
            var account = await _accountRepository.GetAsync(x => x.Id == accountId);

            if (account == null)
            {
                return result;
            }

            var getTokenResult = await _requestService.SendRequestAsync<AuthResponse>(authRequest, true);

            if (!getTokenResult.IsSuccess)
            {
                result.Error = getTokenResult.Error;

                return result;
            }

            string accessToken = getTokenResult.Result.Token.AccessToken;
            var deleteAccountRequest = new DeleteAccountRequest(accessToken, account.YodleeAccountId);
            var deleteAccountResponse = await _requestService.SendRequestAsync<object>(deleteAccountRequest);

            if (!deleteAccountResponse.IsSuccess)
            {
                result.Error = deleteAccountResponse.Error;
                return result;
            }

            List<GroupResult<int>> countsOfAccounts = await _accountRepository.GroupByAsync(a => a.OutsideAssetConnectionId == connectionId, g => g.OutsideAssetConnectionId);

            // todo: refactor this and run under single transaction
            var dbResult = await _holdingsService.DeleteHoldingsAsync(accountId);

            if (!dbResult.Success)
            {
                result.Error = _mapper.Map<RequestError>(dbResult);
                return result;
            }

            // todo: refactor this and run under single transaction
            dbResult = await _accountRepository.DeleteAsync(x => x.Id == accountId);

            if (!dbResult.Success)
            {
                result.Error = _mapper.Map<RequestError>(dbResult);
                return result;
            }

            result.Result.AccounDeleted = true;

            if (countsOfAccounts[0].Count == 1)
            {
                var deleteConnectionResult = await _connectionRepository.DeleteAsync(c => c.Id == connectionId);

                if (!deleteConnectionResult.Success)
                {
                    result.Error = _mapper.Map<RequestError>(deleteConnectionResult);
                    return result;
                }

                result.Result.ConnectionDeleted = true;
            }

            return result;
        }
    }
}
