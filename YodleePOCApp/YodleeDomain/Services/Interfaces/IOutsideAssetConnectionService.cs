﻿using YodleeDomain.Entities;
using YodleeDomain.Models;

namespace YodleeDomain.Services.Interfaces
{
    public interface IOutsideAssetConnectionService
    {
        Task<List<OutsideAssetConnection>> GetConnectionsAsync(int userId);

        Task<RequestResult<OutsideAssetConnection>> SaveConnectionsAsync(EventCallback eventCallback);
    }
}
