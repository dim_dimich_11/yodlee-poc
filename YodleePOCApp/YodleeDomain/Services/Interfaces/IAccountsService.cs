﻿using YodleeDomain.Entities;
using YodleeDomain.Models;
using YodleeDomain.Models.ResponseModels;

namespace YodleeDomain.Services.Interfaces
{
    public interface IAccountsService
    {
        /// <summary>
        /// The get accounts information for specified connection id.
        /// </summary>
        /// <param name="connectionId">The connection id from our database.</param>
        /// <returns></returns>
        Task<List<Account>> GetAccountsAsync(int connectionId);

        Task<DatabaseResult> SaveAccountsAsync(long providerAccountId, string accessToken, int connectionId);

        /// <summary>
        /// The delete account method
        /// </summary>
        /// <param name="accountId">This is account id from our database</param>
        /// <returns></returns>
        Task<RequestResult<DeleteAccountResponse>> DeleteAccountAsync(int accountId, string loginName, int connectionId);
    }
}
