﻿using YodleeDomain.Entities;

namespace YodleeDomain.Services.Interfaces
{
    public interface IHoldingsService
    {
        Task<List<Holding>> GetHoldingsAsync(int accountId);

        Task<DatabaseResult> SaveHoldingsAsync(List<Account> accounts, string accessToken);

        Task<DatabaseResult> DeleteHoldingsAsync(int accountId);
    }
}
