﻿using YodleeDomain.Entities;
using YodleeDomain.Models;

namespace YodleeDomain.Services.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetAllAsync();

        Task<User> GetUserByIdAsync(int userId);

        Task<DatabaseResult> UpdateUserAsync(User user);

        Task<RequestResult<object>> SaveUserAsync(User user);
    }
}
