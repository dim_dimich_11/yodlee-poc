﻿using YodleeDomain.Models;

namespace YodleeDomain.Services.Interfaces
{
    public interface IRequestSenderService
    {
        Task<RequestResult<TResponse>> SendRequestAsync<TResponse>(RequestBase request, bool authorize = false);
    }
}
