﻿using AutoMapper;
using YodleeDomain.Entities;
using YodleeDomain.Models;
using YodleeDomain.Models.RequestModels;
using YodleeDomain.Models.ResponseModels;
using YodleeDomain.Repositories.Interfaces;
using YodleeDomain.Services.Interfaces;

namespace YodleeDomain.Services
{
    public class HoldingsService : IHoldingsService
    {
        private readonly IRepository<Holding> _repository;
        private readonly IRequestSenderService _requestSenderService;
        private readonly IMapper _mapper;

        public HoldingsService(IRequestSenderService requestSenderService, IMapper mapper, IRepository<Holding> repository)
        {
            _requestSenderService = requestSenderService;
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<List<Holding>> GetHoldingsAsync(int accountId)
        {
            var holdings = await _repository.GetAllAsync(x => x.AccountId == accountId);

            return holdings;
        }

        public async Task<DatabaseResult> SaveHoldingsAsync(List<Account> accounts, string accessToken)
        {
            var databaseResult = new DatabaseResult();
            var getHoldingsRequest = new GetHoldingsRequest(accessToken);

            RequestResult<GetHoldingsResponse> getHoldingdResponse = await _requestSenderService.SendRequestAsync<GetHoldingsResponse>(getHoldingsRequest);

            if (!getHoldingdResponse.IsSuccess)
            {
                databaseResult.Exception = new Exception(getHoldingdResponse.Error.ErrorMessage);
                databaseResult.Message = getHoldingdResponse.Error.ErrorMessage;

                return databaseResult;
            }

            var holdings = _mapper.Map<List<Holding>>(getHoldingdResponse.Result.Holding, 
                x => x.AfterMap((source, dest) => 
                {
                    dest.ForEach(h =>
                    {
                        var account = accounts.FirstOrDefault(a => a.YodleeAccountId == h.YodleeAccountId);

                        if (account != null)
                        {
                            h.AccountId = account.Id;
                        }
                    });
                }));

            databaseResult = await _repository.InsertRangeAsync(holdings);

            return databaseResult;
        }

        public async Task<DatabaseResult> DeleteHoldingsAsync(int accountId)
        {
            DatabaseResult databaseResult = await _repository.DeleteRangeAsync(x => x.AccountId == accountId);

            return databaseResult;
        }
    }
}
