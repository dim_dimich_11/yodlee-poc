﻿using AutoMapper;
using YodleeDomain.Entities;
using YodleeDomain.Models;
using YodleeDomain.Models.RequestModels;
using YodleeDomain.Models.ResponseModels;
using YodleeDomain.Repositories.Interfaces;
using YodleeDomain.Services.Interfaces;

namespace YodleeDomain.Services
{
    public class OutsideAssetConnectionService : IOutsideAssetConnectionService
    {
        private readonly IRepository<OutsideAssetConnection> _repository;
        private readonly IRequestSenderService _requestService;
        private readonly IAccountsService _accountsService;
        private readonly IMapper _mapper;

        public OutsideAssetConnectionService(IRepository<OutsideAssetConnection> repository, IRequestSenderService requestService, IMapper mapper, IAccountsService accountsService)
        {
            _repository = repository;
            _requestService = requestService;
            _accountsService = accountsService;
            _mapper = mapper;
        }

        public async Task<List<OutsideAssetConnection>> GetConnectionsAsync(int userId)
        {
            List<OutsideAssetConnection> result = await _repository.GetAllAsync(x => x.UserId == userId, nameof(OutsideAssetConnection.Accounts));

            return result;
        }

        public async Task<RequestResult<OutsideAssetConnection>> SaveConnectionsAsync(EventCallback eventCallback)
        {
            var request = new GetProviderAccountsRequest(eventCallback.AccessToken);
            var response = new RequestResult<OutsideAssetConnection>();

            // check if item has been added already
            var connectionFromDb =
                await _repository.GetAsync(x => x.ProviderAccountId == eventCallback.ProviderAccountId);

            if (connectionFromDb != null)
            {
                response.Result = connectionFromDb;
                return response;
            }

            // todo: replace this with getting info by sending request providersAccounts/{providerAccountId}
            var getResponse = await _requestService.SendRequestAsync<GetProviderAccountsResponse>(request);

            if (!getResponse.IsSuccess)
            {
                return response;
            }

            ProviderAccount provider =
                getResponse.Result.ProviderAccount.FirstOrDefault(x => x.Id == eventCallback.ProviderAccountId);

            if (provider == null)
            {
                response.Error = getResponse.Error;

                return response;
            }

            // move this logic to mapping functionality
            OutsideAssetConnection connection = _mapper.Map<ProviderAccount, OutsideAssetConnection>(provider,
                x => x.AfterMap(
                    (source, destination) =>
                    {
                        YodleeDataset dataset = source.Dataset.FirstOrDefault(ds => ds.Name == "BASIC_AGG_DATA");

                        destination.UserId = eventCallback.UserId;
                        destination.ProviderName = eventCallback.ProviderName;
                        destination.Status = eventCallback.Status;

                        if (dataset != null)
                        {
                            destination.LastUpdated = dataset.LastUpdated == DateTime.MinValue ? null : dataset.LastUpdated;
                            destination.LastUpdatedAttempt = dataset.LastUpdateAttempt == DateTime.MinValue ? null : dataset.LastUpdateAttempt;
                            destination.NextUpdateScheduled = dataset.NextUpdateScheduled == DateTime.MinValue ? null : dataset.NextUpdateScheduled;
                        }
                    }));

            var saveResult = await _repository.InsertAsync(connection);

            if (saveResult.Success)
            {
                var saveAccounts = await _accountsService.SaveAccountsAsync(eventCallback.ProviderAccountId,
                    eventCallback.AccessToken, connection.Id);

                connection.Accounts = null;
                response.Result = connection;
            }
            else
            {
                response.Error = new RequestError
                {
                    ErrorMessage = saveResult.Message,
                    ErrorCode = "DbError"
                };
            }

            return response;
        }
    }
}
