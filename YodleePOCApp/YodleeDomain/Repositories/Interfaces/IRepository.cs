﻿using System.Linq.Expressions;
using YodleeDomain.Entities;

namespace YodleeDomain.Repositories.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, string include);

        Task<TResult> GetMappedResultAsync<TResult>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TResult>> projection);

        Task<List<TResult>> GetMappedResultsAsync<TResult>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TResult>> projection);

        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate, int limit = 20, int skip = 0);

        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate);

        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate, string include = null);

        Task<List<TEntity>> FindManyWithOrderByDescendingAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> sortExpression, int limit = 20, int skip = 0);

        Task<DatabaseResult> InsertAsync(TEntity entity);

        Task<DatabaseResult> InsertRangeAsync(IEnumerable<TEntity> entities);

        Task<DatabaseResult> DeleteAsync(Expression<Func<TEntity, bool>> predicate);

        Task<DatabaseResult> DeleteRangeAsync(Expression<Func<TEntity, bool>> predicate);

        Task<DatabaseResult> UpdateAsync(TEntity entity);

        Task<DatabaseResult> UpdateAsync(TEntity entity, Expression<Func<TEntity, object>> property);

        Task<DatabaseResult> UpdateAsync(List<TEntity> entities);

        Task<bool> AnyAsync();

        Task<List<GroupResult<TResult>>> GroupByAsync<TResult>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TResult>> groupBy);
    }
}
