﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using YodleeDomain.Entities;
using YodleeDomain.Repositories.Interfaces;

namespace YodleeDomain.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var result = await _dbSet.Where(predicate).FirstOrDefaultAsync(predicate);

            return result;
        }

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, string include)
        {
            var result = await _dbSet.Where(predicate).Include(include).FirstOrDefaultAsync();

            return result;
        }

        public async Task<TResult> GetMappedResultAsync<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> projection)
        {
            var result = await _dbSet.Where(predicate).Select(projection).FirstOrDefaultAsync();

            return result;
        }

        public async Task<List<TResult>> GetMappedResultsAsync<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> projection)
        {
            var result = await _dbSet.Where(predicate).Select(projection).ToListAsync();

            return result;
        }

        public async Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate, int limit = 20, int skip = 0)
        {
            var result = await _dbSet.Where(predicate).OrderBy(predicate).Skip(skip).Take(limit).ToListAsync();

            return result;
        }

        public async Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var result = await _dbSet.Where(predicate).ToListAsync();

            return result;
        }

        public async Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate, string include = null)
        {
            var result = await _dbSet.Where(predicate).Include(include).ToListAsync();

            return result;
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            var result = await _dbSet.ToListAsync();

            return result;
        }

        public Task<bool> AnyAsync()
        {
            return _dbSet.AnyAsync();
        }

        public async Task<List<TEntity>> FindManyWithOrderByDescendingAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> sorExpression, int limit = 20, int skip = 0)
        {
            var result = await _dbSet.OrderByDescending(sorExpression).Where(predicate).ToListAsync();

            return result;
        }

        public async Task<DatabaseResult> InsertAsync(TEntity entity)
        {
            var dbResult = new DatabaseResult();
            try
            {
                _dbSet.Add(entity);
                await _context.SaveChangesAsync();

                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = $"Exception in {nameof(InsertAsync)} method.";
                dbResult.Exception = exp;

                return dbResult;
            }
        }

        public async Task<DatabaseResult> InsertRangeAsync(IEnumerable<TEntity> entities)
        {
            var dbResult = new DatabaseResult();
            try
            {
                await _dbSet.AddRangeAsync(entities);
                await _context.SaveChangesAsync();

                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = $"Exception in {nameof(InsertRangeAsync)} method.";
                dbResult.Exception = exp;

                return dbResult;
            }
        }
        public async Task<DatabaseResult> DeleteRangeAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var dbResult = new DatabaseResult();
            try
            {
                var result = _dbSet.Where(predicate);
                _dbSet.RemoveRange(result);
                await _context.SaveChangesAsync();

                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = $"Exception in {nameof(DeleteRangeAsync)} method.";
                dbResult.Exception = exp;

                return dbResult;
            }
        }

        public async Task<DatabaseResult> DeleteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var dbResult = new DatabaseResult();
            try
            {
                var result = await _dbSet.FirstOrDefaultAsync(predicate);
                if (result != null)
                {
                    _dbSet.Remove(result);
                    await _context.SaveChangesAsync();

                    return dbResult;
                }
                
                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = $"Exception in {nameof(DeleteAsync)} method.";
                dbResult.Exception = exp;

                return dbResult;
            }
        }

        public async Task<DatabaseResult> UpdateAsync(TEntity entity)
        {
            var dbResult = new DatabaseResult();
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = $"Exception in {nameof(UpdateAsync)} method.";
                dbResult.Exception = exp;

                return dbResult;
            }

        }

        public async Task<DatabaseResult> UpdateAsync(List<TEntity> entities)
        {
            var dbResult = new DatabaseResult();
            try
            {
                _context.Entry(entities).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = $"Exception in {nameof(UpdateAsync)} method.";
                dbResult.Exception = exp;

                return dbResult;
            }
        }

        public async Task<DatabaseResult> UpdateAsync(TEntity entity, Expression<Func<TEntity, object>> property)
        {
            var dbResult = new DatabaseResult();
            try
            {
                _context.Attach(entity).Property(property).IsModified = true;
                await _context.SaveChangesAsync();

                return dbResult;
            }
            catch (Exception exp)
            {
                dbResult.Message = $"Exception in {nameof(UpdateAsync)} method.";
                dbResult.Exception = exp;

                return dbResult;
            }
        }

        public async Task<List<GroupResult<TResult>>> GroupByAsync<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> groupBy)
        {
            List<GroupResult<TResult>> result = await _dbSet.Where(predicate).GroupBy(groupBy)
                .Select(e => new GroupResult<TResult> { Key = e.Key, Count = e.Count() }).ToListAsync();

            return result;
        }
    }
}
