﻿using YodleeDomain.Enums;

namespace YodleeDomain.Models
{
    public class AssetAggregationConfig
    {
        public string DomainUrl { get; set; }

        public string AdminLoginName { get; set; }

        public string ApiVersion { get; set; }

        public string FastLinkUrl { get; set; }

        public AggregationEnvironment AggregationEnvironment { get; set; }

        public string ClientId { get; set; }

        public string Secret { get; set; }
    }
}
