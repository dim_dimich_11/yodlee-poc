﻿namespace YodleeDomain.Models
{
    public class RequestResult<TResult>
    {
        public RequestError Error { get; set; }

        public bool IsSuccess => Error == null;

        public TResult Result { get; set; }
    }
}
