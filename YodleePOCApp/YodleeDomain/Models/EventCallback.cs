﻿using Newtonsoft.Json;

namespace YodleeDomain.Models
{
    public class EventCallback
    {
        public string AdditionalStatus { get; set; }

        [JsonProperty("isMFAError")]
        public bool IsMFAError { get; set; }

        [JsonProperty("providerAccountId")]
        public long ProviderAccountId { get; set; }

        [JsonProperty("providerId")]
        public long ProviderId { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("providerName")]
        public string ProviderName { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("requestId")]
        public string RequestId { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }
    }
}
