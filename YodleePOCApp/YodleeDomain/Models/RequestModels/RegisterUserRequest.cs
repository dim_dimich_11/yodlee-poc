﻿namespace YodleeDomain.Models.RequestModels
{
    public class RegisterUserRequest : RequestBase
    {
        public RegisterUserRequest(string accessToken) : base(accessToken, Enums.HttpRequestType.Post)
        {
            RequestUrl = Constants.RegisterUser;
        }
    }
}
