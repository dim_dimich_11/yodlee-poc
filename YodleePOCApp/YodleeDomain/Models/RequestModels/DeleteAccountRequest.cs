﻿namespace YodleeDomain.Models.RequestModels
{
    public class DeleteAccountRequest : RequestBase
    {
        public DeleteAccountRequest(string accessToken, long accountId) : base(accessToken, Enums.HttpRequestType.Delete)
        {
            RequestUrl = string.Format(Constants.DeleteAccounts, accountId);
        }
    }
}
