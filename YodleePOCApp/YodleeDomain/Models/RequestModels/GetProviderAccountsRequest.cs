﻿using YodleeDomain.Enums;

namespace YodleeDomain.Models.RequestModels
{
    public class GetProviderAccountsRequest : RequestBase
    {
        public GetProviderAccountsRequest(string accessToken) : base(accessToken, HttpRequestType.Get)
        {
            RequestUrl = Constants.GetProviderAccounts;
            RequestHeaders = new[]
            {
                new KeyValuePair<string, string>("Authorization", $"Bearer {AccessToken}")
            };
        }
    }
}
