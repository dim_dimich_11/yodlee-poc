﻿using Newtonsoft.Json;

namespace YodleeDomain.Models.RequestModels
{
    public class RegisterUserRequestModel
    {
        [JsonProperty("user")]
        public YodleeUser User { get; set; }
    }
}


public class YodleeUser
{
    [JsonProperty("preferences")]
    public Preferences Preferences { get; set; }

    [JsonProperty("loginName")]
    public string LoginName { get; set; }

    [JsonProperty("name")]
    public YodleeUserName Name { get; set; }

    [JsonProperty("email")]
    public string Email { get; set; }

    [JsonProperty("segmentName")]
    public string SegmentName { get; set; } = string.Empty;
}

public class Preferences
{
    [JsonProperty("dateFormat")]
    public string DateFormat { get; set; }

    [JsonProperty("timeZone")]
    public string TimeZone { get; set; }

    [JsonProperty("currency")]
    public string Currency { get; set; }

    [JsonProperty("locale")]
    public string Locale { get; set; }
}

public class YodleeUserName
{
    [JsonProperty("middle")]
    public string Middle { get; set; }

    [JsonProperty("last")]
    public string Last { get; set; }

    [JsonProperty("fullName")]
    public string FullName { get; set; }

    [JsonProperty("first")]
    public string First { get; set; }
}
