﻿using YodleeDomain.Enums;

namespace YodleeDomain.Models.RequestModels
{
    public class GetHoldingsRequest : RequestBase
    {
        public GetHoldingsRequest(string accessToken) : base(accessToken, HttpRequestType.Get)
        {
            RequestUrl = Constants.GetHoldings;
        }
    }
}
