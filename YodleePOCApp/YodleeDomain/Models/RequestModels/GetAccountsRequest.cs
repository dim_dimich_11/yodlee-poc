﻿using YodleeDomain.Enums;

namespace YodleeDomain.Models.RequestModels
{
    public class GetAccountsRequest : RequestBase
    {
        public GetAccountsRequest(string accessToken, long providerAccountId) : base(accessToken, HttpRequestType.Get)
        {
            RequestUrl = Constants.GetAccounts;
            //RequestHeaders = new[]
            //{
            //    new KeyValuePair<string, string>("Authorization", $"Bearer {AccessToken}")
            //};
            RequestUrParameters = new[]
            {
                new KeyValuePair<string, string>("providerAccountId", $"{providerAccountId}")
            };
        }
    }
}
