﻿using YodleeDomain.Enums;

namespace YodleeDomain.Models.RequestModels
{
    public class AuthRequest : RequestBase
    {
        public AuthRequest(string loginName) : base(string.Empty, HttpRequestType.Post, false)
        {
            LoginName = loginName;
            RequestUrl = Constants.AuthRequestUrl;
            RequestHeaders = new[]
            {
                new KeyValuePair<string, string>(Constants.LoginNameKey, loginName)
            };
        }

        public string LoginName { get; set; }
    }
}
