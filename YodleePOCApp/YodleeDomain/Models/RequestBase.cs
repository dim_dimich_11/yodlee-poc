﻿using Newtonsoft.Json;
using YodleeDomain.Enums;

namespace YodleeDomain.Models
{
    public class RequestBase
    {
        /// <summary>
        /// The yodlee endpoint url
        /// </summary>
        [JsonIgnore]
        public string RequestUrl { get; set; }

        /// <summary>
        /// The access token
        /// </summary>
        [JsonIgnore]
        public string AccessToken { get; set; }

        /// <summary>
        /// Current request headers
        /// </summary>
        [JsonIgnore]
        public KeyValuePair<string, string>[] RequestHeaders { get; set; }

        /// <summary>
        /// Request parameters
        /// </summary>
        [JsonIgnore]
        public KeyValuePair<string, string>[] RequestUrParameters { get; set; }

        /// <summary>
        /// The current http request type
        /// </summary>
        [JsonIgnore]
        public HttpRequestType RequestType { get; set; }

        [JsonIgnore]
        /// <summary>
        /// The Request Body Json Value
        /// </summary>
        public string BodyJson { get; set; }

        /// <summary>
        /// The null value handling.
        /// </summary>
        protected NullValueHandling NullValueHandling = NullValueHandling.Ignore;

        public RequestBase(string accessToken, HttpRequestType requestType, bool addTokenHeader = true)
        {
            RequestType = requestType;
            AccessToken = accessToken;

            if (addTokenHeader)
            {
                RequestHeaders = new[]
                {
                    new KeyValuePair<string, string>("Authorization", $"Bearer {AccessToken}")
                };
            }
        }

        /// <summary>
        /// Returns a string that represents the current object in JSON format.
        /// </summary>
        /// <returns>System.String.</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling,
            });
        }
    }
}
