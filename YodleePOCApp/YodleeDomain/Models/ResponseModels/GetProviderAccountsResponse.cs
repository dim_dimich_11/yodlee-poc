﻿namespace YodleeDomain.Models.ResponseModels
{
    public class GetProviderAccountsResponse
    {
        public List<ProviderAccount> ProviderAccount { get; set; }
    }
}
