﻿namespace YodleeDomain.Models.ResponseModels
{
    public  class AuthResponse
    {
        public YodleeTokenModel Token { get; set; }
    }
}
