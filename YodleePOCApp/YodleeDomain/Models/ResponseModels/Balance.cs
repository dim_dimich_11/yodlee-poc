﻿namespace YodleeDomain.Models.ResponseModels
{
    public class Balance
    {
        public string Currency { get; set; }

        public float Amount { get; set; }
    }
}
