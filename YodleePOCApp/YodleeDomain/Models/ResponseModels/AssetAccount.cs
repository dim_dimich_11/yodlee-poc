﻿using Newtonsoft.Json;

namespace YodleeDomain.Models.ResponseModels
{
    public class AssetAccount
    {
        [JsonProperty("CONTAINER")]
        public string Container { get; set; }

        public int ProviderAccountId { get; set; }

        public string AccountName { get; set; }

        public string AccountStatus { get; set; }

        public string AccountNumber { get; set; }

        public string AggregationSource { get; set; }

        public bool SsAsset { get; set; }

        public Balance Balance { get; set; }

        public int Id { get; set; }

        public bool IncludeInNetWorth { get; set; }

        public string ProviderId { get; set; }

        public string ProviderName { get; set; }

        public bool IsManual { get; set; }

        public string AccountType { get; set; }

        public string DisplayedName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string DueDate { get; set; }

        public string InterestRateType { get; set; }

        public string Classification { get; set; }

        public string LastPaymentDate { get; set; }

        public DateTime LastUpdated { get; set; }

        public string MaturityDate { get; set; }

        public Balance OriginalLoanAmount { get; set; }

        public Balance PrincipalBalance { get; set; }

        public string OriginationDate { get; set; }

        public string Frequency { get; set; }

        public Dataset[] Dataset { get; set; }
    }

    public class Dataset
    {
        public string Name { get; set; }

        public string AdditionalStatus { get; set; }

        public string UpdateEligibility { get; set; }

        public DateTime LastUpdated { get; set; }

        public DateTime? LastUpdateAttempt { get; set; }

        public DateTime? NextUpdateScheduled { get; set; }
    }

}
