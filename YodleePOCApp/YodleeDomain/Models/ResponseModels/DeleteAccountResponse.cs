﻿namespace YodleeDomain.Models.ResponseModels
{
    public class DeleteAccountResponse
    {
        public bool AccounDeleted { get; set; }

        public bool ConnectionDeleted { get; set;}
    }
}
