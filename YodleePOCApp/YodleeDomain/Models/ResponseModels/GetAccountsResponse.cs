﻿namespace YodleeDomain.Models.ResponseModels
{
    public class GetAccountsResponse
    {
        public List<AssetAccount> Account { get; set; }
    }
}
