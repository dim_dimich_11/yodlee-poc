﻿namespace YodleeDomain.Models.ResponseModels
{
    public class RegisterUserResponseModel
    {
        public ResponseYodleeUser User { get; set; }
    }

    public class ResponseYodleeUser
    {
        public int Id { get; set; }

        public string LoginName { get; set; }

        public string RoleType { get; set; }
    }
}
