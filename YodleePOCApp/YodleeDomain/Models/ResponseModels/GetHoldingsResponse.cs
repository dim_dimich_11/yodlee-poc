﻿namespace YodleeDomain.Models.ResponseModels
{
    public class GetHoldingsResponse
    {
        public List<HoldingModel> Holding { get; set; }
    }
}
