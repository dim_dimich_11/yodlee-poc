﻿namespace YodleeDomain.Models.ResponseModels
{
    public class YodleeTokenModel
    {
        public string AccessToken { get; set; }

        public DateTime IssuedAt { get; set; }

        public int ExpiresIn { get; set; }
    }
}
