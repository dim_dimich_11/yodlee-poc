﻿namespace YodleeDomain.Models.ResponseModels
{
    public class HoldingModel
    {
        public int Id { get; set; }

        public string HoldingType { get; set; }

        public int ProviderAccountId { get; set; }

        public int AccountId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastUpdated { get; set; }

        public Balance CostBasis { get; set; }

        public string Description { get; set; }

        public string OptionType { get; set; }

        public Balance Price { get; set; }

        public double Quantity { get; set; }

        public string Symbol { get; set; }

        public Balance Value { get; set; }
    }
}
