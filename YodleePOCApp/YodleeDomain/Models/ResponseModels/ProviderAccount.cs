﻿namespace YodleeDomain.Models.ResponseModels
{
    public class ProviderAccount
    {
        public int Id { get; set; }

        public string AggregationSource { get; set; }

        public long ProviderId { get; set; }

        public bool SsManual { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Status { get; set; }

        public List<YodleeDataset> Dataset { get; set; }
    }

    public class YodleeDataset
    {
        public string Name { get; set; }

        public string AdditionalStatus { get; set; }

        public string UpdateEligibility { get; set; }

        public DateTime LastUpdateAttempt { get; set; }

        public DateTime LastUpdated { get; set; }

        public DateTime NextUpdateScheduled { get; set; }
    }
}
