﻿namespace YodleeDomain.Models
{
    public class RequestError
    {
        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public string ReferenceCode { get; set; }
    }
}
