﻿namespace YodleeDomain
{
    public class Constants
    {
        #region Request Paths

        public const string YodleeHttpClient = "YodleeClient";

        public const string AuthRequestUrl = "auth/token";

        public const string GetProviderAccounts = "providerAccounts";

        public const string GetAccounts = "accounts";

        public const string DeleteAccounts = "accounts/{0}";

        public const string GetHoldings = "holdings";

        public const string RegisterUser = "user/register";

        #endregion

        #region Headers Name/Value

        public const string LoginNameKey = "loginName";

        #endregion

        #region Content Types

        public const string ContentApplicationJson = "application/json";

        #endregion

        #region Admin

        public const string AdminLoginName = "3e3085b2-8a0f-448e-8cd5-242acb0a3f1a_ADMIN";

        public const string WebhookUrl = "https://webhook.site/2ea46d55-7eb0-4fe2-89e7-6b2ce2362500";

        #endregion

        #region Default Yodlee Values

        public const string DefaultCurrency = "USD";

        public const string DefaultLocale = "en_US";

        #endregion
    }
}
