﻿using System.ComponentModel.DataAnnotations.Schema;

namespace YodleeDomain.Entities
{
    public class Holding : BaseEntity
    {
        public long YodleeHoldingId { get; set; }

        [ForeignKey("Account")]
        public int AccountId { get; set; }

        public Account AccountModel { get; set; }

        public string HoldingType { get; set; }

        /// <summary>
        /// This is a external id for Account
        /// </summary>
        public long YodleeAccountId { get; set;}

        /// <summary>
        /// This is an external id for Provider Account. In our database we are saving it as OutsideAsset connection model.
        /// </summary>
        public long ProviderAccountId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastUpdated { get; set; }

        public float CostBasisAmount { get; set; }

        public string CostBasisCurrency { get; set; }

        public string Description { get; set; }

        public string OptionType { get; set; }

        public float PriceAmount { get; set; }

        public string PriceCurrency { get; set; }

        public double Quantity { get; set; }

        public string Symbol { get; set; }

        public float ValueAmount { get; set; }

        public string ValueCurrency{ get; set; }
    }
}
