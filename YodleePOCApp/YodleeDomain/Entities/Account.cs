﻿using System.ComponentModel.DataAnnotations.Schema;

namespace YodleeDomain.Entities
{
    public class Account : BaseEntity
    {
        [ForeignKey("OutsideAssetConnection")]
        public int OutsideAssetConnectionId { get; set; }

        public OutsideAssetConnection OutsideAssetConnection { get; set; }

        /// <summary>
        /// This is an id from asset provider service
        /// </summary>
        public long YodleeAccountId { get; set; }

        public string AccountName { get; set; }

        public string AccountStatus { get; set; }

        public string AccountNumber { get; set; }

        public string AggregationSource { get; set; }

        public string Currency { get; set; }

        public float Amount { get; set; }

        public bool IncludeInNetWorth { get; set; }

        public bool IsManual { get; set; }

        public string AccountType { get; set; }

        public string DisplayedName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Classification { get; set; }
        
        public DateTime LastUpdated { get; set; }

        public string MaturityDate { get; set; }

        public DateTime? LastUpdateAttempt { get; set; }

        public DateTime? NextUpdateScheduled { get; set; }
    }
}
