﻿namespace YodleeDomain.Entities
{
    public class DatabaseResult
    {
        public bool Success => Exception == null;

        public string Message { get; set; } = string.Empty;

        public Exception Exception { get; set; } = null;
    }
}
