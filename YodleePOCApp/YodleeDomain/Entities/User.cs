﻿using Microsoft.EntityFrameworkCore;

namespace YodleeDomain.Entities
{
    [Index(nameof(NickName))]
    public class User : BaseEntity
    {
        public string Name { get; set; }

        public string NickName { get; set; }

        public string Email { get; set; }

        public string AccessToken { get; set; }

        public int TokenExpirationTime { get; set; }

        public int YodleeUserId { get; set; }

        public string YodleeUserLoginName { get; set; }

        public string YodleeUserRoleType { get; set; }

        public string WebhookCallbackUrl { get; set; }
    }
}
