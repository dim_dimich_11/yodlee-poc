﻿namespace YodleeDomain.Entities
{
    public class GroupResult<T>
    {
        public T Key { get; set; }

        public int Count { get; set; }
    }
}
