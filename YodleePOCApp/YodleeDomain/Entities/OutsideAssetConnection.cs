﻿using System.ComponentModel.DataAnnotations.Schema;

namespace YodleeDomain.Entities
{
    public class OutsideAssetConnection : BaseEntity
    {
        [ForeignKey("User")]
        public int UserId { get; set; }

        public string ProviderName { get; set; }

        public long ProviderId { get; set; }

        public long ProviderAccountId { get; set; }

        public string Status { get; set; }

        [Column("LastUpdated", TypeName = "DateTime")]
        public DateTime? LastUpdated { get; set; } = null;

        [Column("LastUpdatedAttempt", TypeName = "DateTime")]
        public DateTime? LastUpdatedAttempt { get; set; } = null;

        [Column("NextUpdateScheduled", TypeName = "DateTime")]
        public DateTime? NextUpdateScheduled { get; set; } = null;

        public string UpdateEligibility { get; set; }

        public List<Account> Accounts { get; set; }
    }
}
