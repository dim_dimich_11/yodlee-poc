﻿using Microsoft.EntityFrameworkCore;
using YodleeDomain.Configuration;

namespace YodleeDomain.Entities
{
    public class AssetAggregationContext : DbContext
    {
        public AssetAggregationContext(DbContextOptions<AssetAggregationContext> options) : base(options)
        {
            ChangeTracker.AutoDetectChangesEnabled = false;
            ChangeTracker.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserBaseConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<OutsideAssetConnection> OutsideAssetConnections { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Holding> Holdings { get; set; }
    }
}
