﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YodleeDomain.Entities;

namespace YodleeDomain.Configuration
{
    public class UserBaseConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            //builder.ToTable("Users");
            //builder.Property(s => s.Name).IsRequired();
            //builder.Property(s => s.Email).IsRequired();

            builder.HasData
            (
                new User
                {
                    Id = 1,
                    Name = "TestAccount_1",
                    Email = "testac1@testem.com",
                    AccessToken = "empty_value",
                    NickName = "test_acc_1"
                },
                new User
                {
                    Id = 2,
                    Name = "TestAccount_2",
                    Email = "testac2@testem.com",
                    AccessToken = "empty_value",
                    NickName = "test_acc_3"
                },
                new User
                {
                    Id = 3,
                    Name = "TestAccount_3",
                    Email = "testac3@testem.com",
                    AccessToken = "empty_value",
                    NickName = "test_acc_3"
                }
            );
        }
    }
}
