﻿namespace YodleeDomain.Extensions
{
    public static class StringExtensions
    {
        public static string AddUrlParameters(this string url, KeyValuePair<string, string>[] parameters)
        {
            if (parameters == null)
            {
                return url;
            }

            for (int i = 0; i < parameters.Length; i++)
            {
                var parameter = parameters[i];

                if (i == 0)
                {
                    url = $"{url}?{parameter.Key}={parameter.Value}";
                    continue;
                }

                url = $"&{url}{parameter.Key}={parameter.Value}";
            }

            return url;
        }
    }
}
