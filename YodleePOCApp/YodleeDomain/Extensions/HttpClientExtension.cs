﻿namespace YodleeDomain.Extensions
{
    public static class HttpClientExtension
    {
        public static void AddHeaders(this HttpClient client, KeyValuePair<string, string>[] headers)
        {
            if (headers == null)
            {
                return;
            }

            foreach (var header in headers)
            {
                var result = client.DefaultRequestHeaders.TryGetValues(header.Key, out IEnumerable<string> values);

                if (!result)
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }
        }
    }
}
