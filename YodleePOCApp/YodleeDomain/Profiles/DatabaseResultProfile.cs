﻿using AutoMapper;
using YodleeDomain.Entities;
using YodleeDomain.Models;

namespace YodleeDomain.Profiles
{
    public class DatabaseResultProfile : Profile
    {
        public DatabaseResultProfile()
        {
            CreateMap<DatabaseResult, RequestError>()
                .ForMember(dest => dest.ErrorCode, opt => opt.MapFrom(_ => "400"))
                .ForMember(dest => dest.ErrorMessage, opt => opt.MapFrom(source => source.Message))
                .ForMember(dest => dest.ReferenceCode, opt => opt.MapFrom(_ => string.Empty));
        }
    }
}
