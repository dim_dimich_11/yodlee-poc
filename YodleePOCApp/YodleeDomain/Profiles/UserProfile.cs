﻿using AutoMapper;
using YodleeDomain.Entities;

namespace YodleeDomain.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, YodleeUser>()
                .ForMember(x => x.Email, opt => opt.MapFrom(source => source.Email))
                .ForMember(x => x.LoginName, opt => opt.MapFrom(source => source.NickName))
                .ForMember(x => x.Preferences, opt => opt.MapFrom(source => new Preferences
                {
                    Currency = Constants.DefaultCurrency,
                    Locale = Constants.DefaultLocale
                }))
                .ForMember(x => x.SegmentName, opt => opt.MapFrom(source => string.Empty))
                .ForMember(x => x.Name, opt => opt.MapFrom(source => new YodleeUserName
                {
                    FullName = source.Name,
                    First = string.Empty,
                    Last = string.Empty,
                    Middle = string.Empty
                }));
        }
    }
}
