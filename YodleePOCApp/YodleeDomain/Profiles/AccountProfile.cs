﻿using AutoMapper;
using YodleeDomain.Entities;
using YodleeDomain.Models.ResponseModels;

namespace YodleeDomain.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<AssetAccount, Account>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.OutsideAssetConnectionId, opt => opt.Ignore())
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(dest => dest.Balance != null ? dest.Balance.Amount : 0))
                .ForMember(dest => dest.Currency, opt => opt.MapFrom(dest => dest.Balance != null ? dest.Balance.Currency : null))
                .ForMember(dest => dest.YodleeAccountId, opt => opt.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.LastUpdateAttempt, opt => opt.MapFrom(dest => GetLastUpdateAttemptDate(dest)))
                .ForMember(dest => dest.NextUpdateScheduled, opt => opt.MapFrom(dest => GetNextUpdateDate(dest)));
        }

        private DateTime? GetLastUpdateAttemptDate(AssetAccount accuont)
        {
            return accuont.Dataset.Any() && accuont.Dataset[0].LastUpdateAttempt.HasValue ? accuont.Dataset[0].LastUpdateAttempt.Value : null;
        }

        private DateTime? GetNextUpdateDate(AssetAccount accuont)
        {
            return accuont.Dataset.Any() && accuont.Dataset[0].NextUpdateScheduled.HasValue ? accuont.Dataset[0].NextUpdateScheduled.Value : null;
        }
    }
}
