﻿using AutoMapper;
using YodleeDomain.Entities;
using YodleeDomain.Models.ResponseModels;

namespace YodleeDomain.Profiles
{
    public class HoldingProfile : Profile
    {
        public HoldingProfile()
        {
            CreateMap<HoldingModel, Holding>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.AccountId, opt => opt.Ignore())
                .ForMember(dest => dest.YodleeHoldingId, opt => opt.MapFrom(source => source.Id))
                .ForMember(dest => dest.YodleeAccountId, opt => opt.MapFrom(source => source.AccountId))
                .ForMember(dest => dest.CostBasisAmount, opt => opt.MapFrom(source => source.CostBasis.Amount))
                .ForMember(dest => dest.CostBasisCurrency, opt => opt.MapFrom(source => source.CostBasis.Currency))
                .ForMember(dest => dest.PriceAmount, opt => opt.MapFrom(source => source.Price.Amount))
                .ForMember(dest => dest.PriceCurrency, opt => opt.MapFrom(source => source.Price.Currency))
                .ForMember(dest => dest.ValueAmount, opt => opt.MapFrom(source => source.Value.Amount))
                .ForMember(dest => dest.ValueCurrency, opt => opt.MapFrom(source => source.Value.Currency));
        }
    }
}
