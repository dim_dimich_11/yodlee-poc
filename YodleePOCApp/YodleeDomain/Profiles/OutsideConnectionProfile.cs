﻿using AutoMapper;
using YodleeDomain.Entities;
using YodleeDomain.Models.ResponseModels;

namespace YodleeDomain.Profiles
{
    public class OutsideConnectionProfile : Profile
    {
        public OutsideConnectionProfile()
        {
            CreateMap<ProviderAccount, OutsideAssetConnection>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.ProviderId, opt => opt.MapFrom(src => src.ProviderId))
                .ForMember(dest => dest.ProviderAccountId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status));
        }
    }
}
