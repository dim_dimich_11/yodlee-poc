using AutoMapper;
using Microsoft.EntityFrameworkCore;
using YodleeDomain;
using YodleeDomain.Entities;
using YodleeDomain.Models;
using YodleeDomain.Profiles;
using YodleeDomain.Repositories;
using YodleeDomain.Repositories.Interfaces;
using YodleeDomain.Services;
using YodleeDomain.Services.Interfaces;
using YodleePOCApp.Constants;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.Configure<AssetAggregationConfig>(builder.Configuration.GetSection(PocConstants.AssetAggregationConfigKey));

string dbConnection = builder.Configuration.GetValue<string>("ConnectionStrings:YodleeDbConnection");
builder.Services.AddDbContext<DbContext, AssetAggregationContext>(options =>
    options.UseSqlServer(dbConnection,
        config => 
        { config.EnableRetryOnFailure(5, TimeSpan.FromSeconds(60), null); 
        }));

// repositories
builder.Services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));

// services
builder.Services.AddScoped<IRequestSenderService, RequestSenderService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IOutsideAssetConnectionService, OutsideAssetConnectionService>();
builder.Services.AddScoped<IAccountsService, AccountsService>();
builder.Services.AddScoped<IHoldingsService, HoldingsService>();

// automaper profiles
var mapConfig = new MapperConfiguration(_ =>
{
    _.AddProfile<OutsideConnectionProfile>();
    _.AddProfile<AccountProfile>();
    _.AddProfile<DatabaseResultProfile>();
    _.AddProfile<HoldingProfile>();
    _.AddProfile<UserProfile>();
});

IMapper mapper = mapConfig.CreateMapper();

builder.Services.AddSingleton(mapper);

// http client
builder.Services.AddHttpClient(Constants.YodleeHttpClient, c =>
{
    c.BaseAddress = new Uri(builder.Configuration.GetValue<string>("AssetAggregationConfig:DomainUrl"));
    c.DefaultRequestHeaders.Add("Api-Version", builder.Configuration.GetValue<string>("AssetAggregationConfig:ApiVersion"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.UseEndpoints(x =>
{
    x.MapControllerRoute("default", "{controller=User}/{action=Index}/{id?}");
});

//app.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");

//app.MapControllers();

app.MapRazorPages();

app.Run();
