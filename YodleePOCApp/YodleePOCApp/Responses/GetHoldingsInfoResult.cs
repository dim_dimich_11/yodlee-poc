﻿using YodleeDomain.Entities;

namespace YodleePOCApp.Responses
{
    public class GetHoldingsInfoResult
    {
        public string AccountName { get; set; }

        public List<Holding> Holdings { get; set; }
    }
}
