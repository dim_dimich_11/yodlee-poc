﻿using YodleeDomain.Entities;

namespace YodleePOCApp.Responses
{
    public class GetInstitutionInfoResult
    {
        public OutsideAssetConnection Connection { get; set; }

        public List<Account> Accounts { get; set; } = new List<Account>();
    }
}
