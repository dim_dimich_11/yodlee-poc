﻿using YodleeDomain.Entities;

namespace YodleePOCApp.Responses
{
    public class GetInstitutionResult
    {
        public List<OutsideAssetConnection> Institutions { get; set; }

        public int UserId { get; set; }

        public string UserNickName { get; set; }
    }

    public class InstitutionViewModel
    {
        public OutsideAssetConnection ConnectionInfo { get; set; }

        public int CountOfAccounts { get; set; }
    }
}
