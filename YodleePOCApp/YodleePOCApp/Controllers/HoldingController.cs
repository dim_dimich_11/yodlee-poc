﻿using Microsoft.AspNetCore.Mvc;
using YodleeDomain.Services.Interfaces;
using YodleePOCApp.Responses;

namespace YodleePOCApp.Controllers
{
    public class HoldingController : Controller
    {
        private readonly IHoldingsService _holdingsService;

        public HoldingController(IHoldingsService holdingsService)
        {
            _holdingsService = holdingsService;
        }

        [HttpGet]
        public async Task<IActionResult> HoldingDetailsAsync(int accountId, string accountName)
        {
            var result = new GetHoldingsInfoResult();
            result.Holdings = await _holdingsService.GetHoldingsAsync(accountId);
            result.AccountName = accountName;

            return View(result);
        }
    }
}
