﻿using Microsoft.AspNetCore.Mvc;
using YodleeDomain.Models.RequestModels;
using YodleeDomain.Models.ResponseModels;
using YodleeDomain.Services.Interfaces;

namespace YodleePOCApp.Controllers
{
    public class AdminController : Controller
    {
        private readonly IRequestSenderService _requestService;

        public AdminController(IRequestSenderService requestService)
        {
            _requestService = requestService;
        }
        
        public async Task<IActionResult> GetTokenAsync(string loginName)
        {
            // todo: before getting the access token we should register user and subscribe to events
            var authRequest = new AuthRequest(loginName);
            var result = await _requestService.SendRequestAsync<AuthResponse>(authRequest, true);

            return result.IsSuccess ? Ok(result) : BadRequest(result);
        }
    }
}
