﻿using Microsoft.AspNetCore.Mvc;
using YodleeDomain.Models;
using YodleeDomain.Services.Interfaces;
using YodleePOCApp.Responses;

namespace YodleePOCApp.Controllers
{
    public class InstitutionController : Controller
    {
        private readonly IOutsideAssetConnectionService _connectionService;
        private readonly IAccountsService _accountsService;

        public InstitutionController(IOutsideAssetConnectionService connectionService, IAccountsService accountsService)
        {
            _connectionService = connectionService;
            _accountsService = accountsService;
        }

        [HttpGet]
        public async Task<IActionResult> UserInstitutionsAsync(string loginName, int userId)
        {
            var connections = await _connectionService.GetConnectionsAsync(userId);
            var result = new GetInstitutionResult
            {
                Institutions = connections,
                UserNickName = loginName,
                UserId = userId
            };

            return View(result);
        }

        [HttpGet]
        public async Task<IActionResult> InstitutionDetailsAsync(int connectionId)
        {
            var result = new GetInstitutionInfoResult();

            var accounts = await _accountsService.GetAccountsAsync(connectionId);

            if (accounts.Any())
            {
                result.Accounts = accounts;
                result.Connection = accounts[0].OutsideAssetConnection;
            }

            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddAsync([FromBody]EventCallback eventCallback)
        {
            var result = await _connectionService.SaveConnectionsAsync(eventCallback);
            return result.IsSuccess ? Ok(result) : BadRequest(result);
        }
    }
}
