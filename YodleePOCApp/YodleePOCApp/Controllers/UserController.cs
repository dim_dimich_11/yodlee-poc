﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using YodleeDomain.Entities;
using YodleeDomain.Models;
using YodleeDomain.Services.Interfaces;

namespace YodleePOCApp.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var model = await _userService.GetAllAsync();

            return PartialView("_Users", model);
        }

        public async Task<IActionResult> SaveUserAsync([FromBody]User userModel)
        {
            RequestResult<object> result;
            try
            {
                result = await _userService.SaveUserAsync(userModel);
            }
            catch (Exception ex)
            {
                result = new RequestResult<object>
                {
                    Error = _mapper.Map<RequestError>(ex)
                };
            }

            return result.IsSuccess ? Ok(result) : BadRequest(result);
        }
    }
}
