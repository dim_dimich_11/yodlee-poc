﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using YodleeDomain.Services.Interfaces;

namespace YodleePOCApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountsService _accountService;

        public AccountController(IAccountsService accountService)
        {
            _accountService = accountService;
        }

        public async Task<IActionResult> DeleteAsync(int accountId, int connectionId)
        {
            bool result = Request.Headers.TryGetValue("loginName", out StringValues loginName);

            if (!result)
            {
                return BadRequest("Invalid login name.");
            }

            var requestResult = await _accountService.DeleteAccountAsync(accountId, loginName.ToString(), connectionId);

            return requestResult.IsSuccess ? Ok(requestResult) : BadRequest(requestResult);
        }
    }
}
