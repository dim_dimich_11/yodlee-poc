﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

var selectedLoginName = localStorage.getItem("selectedLoginName");
var selectedUserId = localStorage.getItem("selectedUserId");

var requestService = {
    post: function (url, data, callback) {
        var request = new XMLHttpRequest();
        request.open("POST", url, true);
        request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (callback && !this.responseText) {
                    callback("");
                } else if (callback) {
                    callback(JSON.parse(this.responseText));
                }
            }
        };

        request.send(JSON.stringify(data));
    },

    get: function (url, callback) {
        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (callback) { callback(JSON.parse(this.responseText)); }
            }
        };
        request.send();
    },
    delete: function (url, callback) {
        var request = new XMLHttpRequest();
        request.open("DELETE", url, true);
        request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        request.setRequestHeader("loginName", selectedLoginName);
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (callback) { callback(JSON.parse(this.responseText)); }
            }
        };
        request.send();
    },
    getView: function (url, callback) {
        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.setRequestHeader("Accept", "text / xml");
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (callback) { callback(this.responseText); }

            }
        };
        request.send();
    },
    showPopup: function (message) {
        console.log("popup message:" + message);
    }
};

function excludeAccount(event, accountId, connectionId) {
    event.stopPropagation();
    let path = `/account/delete?accountId=${accountId}&connectionId=${connectionId}`;

    requestService.delete(path, response => {
        if (response.isSuccess) {
            if (response.result.connectionDeleted) {
                window.location.href = `${window.location.origin}/Institution/UserInstitutions?loginName=${selectedLoginName}&userId=${selectedUserId}`; // navigate to UserInstitutions page
            } else {
                location.reload();
            }
        }
    });

}